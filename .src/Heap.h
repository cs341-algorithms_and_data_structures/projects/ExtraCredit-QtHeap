#include <iostream>
#include <cmath> 
using namespace std;

template <class K, class V > class Heap; //prototype

template <class K, class V >
class Node {
	public:
		friend class Heap <K, V>;
		friend class MainWindow;
		Node <K, V> *par;
		Node <K, V> *left;
		Node <K, V> *right;

		K key;
		V val;	
};

template<class K, class V>
class Heap{
	public:
		friend class MainWindow;
		Heap(){}
		Heap(Node<K,V>*);
		~Heap();
		void makeHeap(Node<K,V>*,int); //link nodes
		void maxHeapify(Node<K,V>*, int); //max-heapify
		void swapN(Node<K,V>&, Node<K,V>&); //swap data
		void printHeap(Node<K,V>*,int); //show heap structure
	private:
		Node<K,V> *root; //store address of heap
};
