#include "mainwindow.h"
#include "ui_mainwindow.h"

int MainWindow::ndCtr=1;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
			ui(new Ui::MainWindow), scene(new QGraphicsScene(this)),
			heapPtr(new Node<int,string>), xPtr(new int), yPtr(new int),
			hp(new Heap<int,string>){

	ui->setupUi(this);
}

MainWindow::~MainWindow(){
	delete ui;
}

/*Stores Pointers for Heap and Ellipse Coordinate Arrays*/
void MainWindow::setArrPtr(Node<int,string> *hArr, const int *x,const int *y){
	heapPtr= hArr;
	xPtr=x;
	yPtr=y;
}

/*Links Nodes With Appropriate Children and Parent*/
void MainWindow::linkNodes(){
	hp->makeHeap(heapPtr,10);
}

/*Displays Initial Tree*/
void MainWindow::on_btnLoadHeap_clicked(){
	aNode *nodex;

	ui->visualTree->setScene(scene); //initialize canvas

	/*Display all Nodes*/
	for(int ctr=1; ctr<=10; ++ctr){
		nodex= new aNode(heapPtr[ctr].val,heapPtr[ctr].key,
						 xPtr[ctr-1], yPtr[ctr-1]);

		scene->addItem(nodex);
	}
}

/*Displays a Student Node*/
void MainWindow::on_btnShowEachNode_clicked(){
	QSound::play("cowbell.wav");

	aNode *nodex;

	ui->visualTree->setScene(scene); //initialize canvas

	/*Create a single ellipse containing name and priority key*/
	if (ndCtr>=1 && ndCtr<=10){
		nodex= new aNode(heapPtr[ndCtr].val, heapPtr[ndCtr].key,
						 xPtr[ndCtr-1],yPtr[ndCtr-1]);

		scene->addItem(nodex); //display node
	}else{
		QMessageBox msgBox;
		msgBox.setText("No More Nodes.");
		msgBox.exec();
	}

	++ndCtr;
}

/*Clear Canvas and Restructure Tree Into Max-Heap*/
void MainWindow::on_btnBuildMaxHeap_clicked(){
	scene->clear();
	ndCtr=1;

	/*Order Into Max-Heap*/
	for(int ctr=5; ctr>1; --ctr){
		hp->maxHeapify(&heapPtr[0],10);
	}
}
