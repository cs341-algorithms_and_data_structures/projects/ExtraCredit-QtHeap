#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QMessageBox>
#include <QtMultimedia/QSound>

#include <iostream>
#include <Heap.h>
using namespace std;

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();

		//store heap array address and size
		void setArrPtr(Node<int,string> *hArr, const int *x,const int *y);
		void linkNodes();
	protected:

	private slots:
		void on_btnLoadHeap_clicked();		//display initial tree structure
		void on_btnShowEachNode_clicked();	//display next node
		void on_btnBuildMaxHeap_clicked();	//clear canvas and build max heap

	private:
		Ui::MainWindow *ui;
		QGraphicsScene *scene; 				//panel to display heap nodes
		Node<int,string> *heapPtr;			//points to heap array
		const int *xPtr; 					//x coordinates of ellipses
		const int *yPtr;					//y coordinates of ellipses

		static int ndCtr;					//number of ellipses on display
		Heap<int,string> *hp;				//heap class for operations
};

class  aNode : public QGraphicsItem{
	private:
		int key, x, y, width=100, height=50;
		string name;

	public:
		aNode(string name,int key,int x,int y) :name(name), key(key), x(x), y(y){}

		QRectF boundingRect() const{
			qreal penWidth = 1;
			return QRectF(-10-(penWidth/2), -10-(penWidth/2), 20+penWidth, 20+penWidth);
		}

		void paint(QPainter *picasso, const QStyleOptionGraphicsItem *option,QWidget *widget){
			QRectF rectlipse(x,y,width,height);

			picasso->drawText(rectlipse,Qt::AlignHCenter, QVariant(key).toString());
			picasso->drawText(rectlipse, Qt::AlignCenter,name.c_str());
			picasso->drawEllipse (rectlipse);
		}
};




#endif // MAINWINDOW_H
