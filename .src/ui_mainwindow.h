/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *btnLoadHeap;
    QGraphicsView *visualTree;
    QPushButton *btnBuildMaxHeap;
    QPushButton *btnShowEachNode;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1102, 704);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        btnLoadHeap = new QPushButton(centralWidget);
        btnLoadHeap->setObjectName(QStringLiteral("btnLoadHeap"));
        btnLoadHeap->setGeometry(QRect(40, 20, 311, 101));
        visualTree = new QGraphicsView(centralWidget);
        visualTree->setObjectName(QStringLiteral("visualTree"));
        visualTree->setGeometry(QRect(40, 130, 1021, 531));
        sizePolicy.setHeightForWidth(visualTree->sizePolicy().hasHeightForWidth());
        visualTree->setSizePolicy(sizePolicy);
        visualTree->setAutoFillBackground(false);
        visualTree->setFrameShape(QFrame::WinPanel);
        visualTree->setLineWidth(3);
        visualTree->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        visualTree->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
        btnBuildMaxHeap = new QPushButton(centralWidget);
        btnBuildMaxHeap->setObjectName(QStringLiteral("btnBuildMaxHeap"));
        btnBuildMaxHeap->setGeometry(QRect(400, 20, 311, 101));
        btnShowEachNode = new QPushButton(centralWidget);
        btnShowEachNode->setObjectName(QStringLiteral("btnShowEachNode"));
        btnShowEachNode->setGeometry(QRect(750, 20, 311, 101));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Heap Visuallizer", Q_NULLPTR));
        btnLoadHeap->setText(QApplication::translate("MainWindow", "Show Full Tree", Q_NULLPTR));
        btnBuildMaxHeap->setText(QApplication::translate("MainWindow", "Build Max Heap", Q_NULLPTR));
        btnShowEachNode->setText(QApplication::translate("MainWindow", "Show Each Node", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
