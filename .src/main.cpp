#include "mainwindow.h"
#include <QApplication>

#include <iostream>
#include <random> //mt19937 random
#include <chrono> //seed mt19937
#include <iomanip>
//#include "Heap.h"
using namespace std;

void randKeys(int*, int, int, int);
void printKeys(int*, int);

int main(int argc, char *argv[]){
	QApplication a(argc, argv);

	int nKeys= 10, low= 1, high= 100;
	int *keyArr= new int[nKeys];	//first node at index 1
	string *nameArr= new string[nKeys] {"Joe", "Schmo", "Mary", "Jane",	"Peter",
		"Piper", "Arnold", "Guvnor", "Rob", "Sneider"};

	/*Coordinates for the positions of each node*/
	int xCoord[10]= {-50,-275,225,-400,-150,100,350,-462,-337,-215};
	int yCoord[10]= {0,100,100,200,200,200,200,300,300,300};

	/*Generate random priority keys*/
	randKeys(keyArr, nKeys, low, high);

	/*Create Heap Structure*/
	Node <int, string> *heapArr= new Node<int,string>[nKeys+1]; //heap starts at index 1

	/*Create Nodes*/
	for(int ctr=0; ctr<nKeys ;++ctr){
		Node<int, string> temp;	//temporary node
		temp.key= keyArr[ctr];
		temp.val= nameArr[ctr];
		heapArr[ctr+1]= temp;
	}

	/*Begin GUI*/
	MainWindow mw(0);
	mw.setArrPtr(heapArr,xCoord,yCoord);
	mw.linkNodes();

	/**/
	mw.show();

	return a.exec();
}

/*Randomize x number of integers between two bounds*/
void randKeys(int *keyArr, int quantity, int low, int high){
	/*Create a seed*/
	auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();

	/*Seed the randomizer and limit ranges*/
	mt19937 mt(seed);
	uniform_int_distribution<int> ran(low, high);

	/*Generate x numbers and place in array*/
	for (int ctr=0; ctr<quantity; ++ctr) {
		*keyArr = ran(mt);
		++keyArr;
	}
}
