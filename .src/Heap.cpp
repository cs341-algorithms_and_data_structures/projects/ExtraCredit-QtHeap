#ifndef HEAP_H
#define HEAP_H

#include "Heap.h"

/*Constructor*/
template<class K, class V>
Heap<K,V>::Heap(Node<K,V> *r) : root(r){
	cout << "\n\n\t!! Heap Status: Heap was created !!\n\n";
}

/*Destructor*/
template<class K, class V>
Heap<K,V>::~Heap(){
	cout << "\n\n  !! Heap Status: Heap was destroyed !! \n\n";
}

/*Create Heap Links*/
template<class K, class V>
void Heap<K,V>::makeHeap(Node<K,V> *arr, int nKeys){
	for(int ctr=1; ctr<=(nKeys); ++ctr){
		if(ctr>1)	//arr node doesnt get parent
			arr[ctr].par= &arr[ctr/2];

		//only give children to nodes with children
		if(ctr <= (nKeys/2)){ 
			arr[ctr].left= &arr[2*ctr];	//left child
		
			//only link right child if exists
			if(&arr[2*ctr+1] != NULL)
				arr[ctr].right= &arr[(2*ctr)+1]; //right child
		}
	}
}

/*Heapify the Heap into Max Heap*/
template<class K, class V>
void Heap<K,V>::maxHeapify(Node<K,V> *arr, int aSize){
	for(int ctr=aSize/2 ; ctr>=1; --ctr){
		Node<K,V> temp;

		//Right Child Exists
		if(arr[(2*ctr+1)].key > 0){			
			if(arr[ctr].left->key < arr[ctr].right->key){
				//if parent smaller than right, swap
				if(arr[ctr].key < arr[ctr].right->key){ 
					swapN(arr[ctr], *arr[ctr].right);
					printHeap(arr,aSize);
				}

			}else if(arr[ctr].key < arr[ctr].left->key){
				//if parent smaller than left, swap
				swapN(arr[ctr], *arr[ctr].left);
				printHeap(arr,aSize);
			}
		
		//No Right Child
		}else if(arr[ctr].key < arr[ctr].left->key){
			//if parent smaller than left, swap
			swapN(arr[ctr], *arr[ctr].left);
			printHeap(arr,aSize);
		}
	}
}

/*Swap Node Data*/
template<class K, class V>
void Heap<K,V>::swapN(Node<K,V> &a, Node<K,V> &b)
{
	cout<<"\n...swapping key ("<<a.key<<") with ("<<b.key<<")"; 

    Node<K,V> temp = a;
    a.key = b.key;
	a.val = b.val;
    
	b.key = temp.key;
	b.val = temp.val;
}

/*Display Graphical Heap Structure*/
template<class K, class V>
void Heap<K,V>::printHeap(Node<K,V> *arr, int nodeCount){
	int level= 1 + (int)floor( log(nodeCount)/log(2) ); //levels in the heapa

	//print one level at a time with proper node spacing
	cout<<"\n\n";
	while(level>0){
		if(level==4) 
			cout<<"\t\t\t\t"<<arr[1].key<<endl;
		else if(level==3)
			cout<<"\t\t"<<arr[2].key<<"\t\t\t\t"<<arr[3].key<<endl;
		else if(level==2)
			cout<<"\t"<<arr[4].key<<"\t\t"<<arr[5].key<<"\t\t"<<arr[6].key
				<<"\t\t"<<arr[7].key<<endl;
		else if(level==1)
			cout<<"  "<<arr[8].key<<"   \t"<<arr[9].key<<"\t"<<arr[10].key<<endl;
		--level;	
	}
}

//Explicitly instantiate template classes
template class Node <int, string> ;
template class Heap <int, string> ;

#endif
